import pandas as pd
import pickle

from sklearn.linear_model import LinearRegression


data_set = pd.read_csv('hiring.csv')

data_set['experience'].fillna(0, inplace=True)

data_set['test_score'].fillna(data_set['test_score'].mean(), inplace=True)

X = data_set.iloc[:, :3]


def convert_to_int(word):
    word_dict = \
        {'one': 1, 'two': 2, 'three': 3, 'four': 4, 'five': 5, 'six': 6,
         'seven': 7, 'eight': 8, 'nine': 9, 'ten': 10, 'eleven': 11,
         'twelve': 12, 'zero': 0, 0: 0
         }
    return word_dict[word]


X['experience'] = X['experience'].apply(lambda x: convert_to_int(x))
y = data_set.iloc[:, -1]

regressor = LinearRegression()

regressor.fit(X, y)

# Saving model to disk
pickle.dump(regressor, open('model.pkl', 'wb'))

# Loading model to compare the results
model = pickle.load(open('model.pkl', 'rb'))
print(model.predict([[2, 9, 6]]))
